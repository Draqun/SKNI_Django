from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^', include('SKNI_Django.Index.urls')),
    url(r'^', include('SKNI_Django.Author.urls')),
)
