from django.db import models

class Author(models.Model):
  first_name = models.CharField(max_length=64)
  last_name = models.CharField(max_length=64)

class Book(models.Model):
  title = models.CharField(max_length=64)
  author = models.ForeignKey(Author)
  
class Client(models.Model):
  first_name = models.CharField(max_length=64)
  last_name = models.CharField(max_length=64)
  address = models.TextField()
  
class BorrowBooks(models.Model):
  client = models.ForeignKey(Client)
  book = models.ForeignKey(Book)