from django.views.generic import CreateView, UpdateView, DeleteView, ListView
from SKNI_Django.Author.models import Author
from SKNI_Django.Author.forms import CreateAuthorModelForm
from django.core.urlresolvers import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin

class BooksAuthors(ListView):
  template_name = "booksAuthors.html"
  model = Author
  
booksAuthors = BooksAuthors.as_view()

class CreateBookAuthor(SuccessMessageMixin, CreateView):
  template_name = "createAuthor.html"
  form_class = CreateAuthorModelForm
  model = Author
  success_url = reverse_lazy("booksAuthors")
  success_message = u"%(first_name)s %(last_name)s is added to database."
  
createBookAuthor = CreateBookAuthor.as_view()
  
class UpdateBookAuthor(SuccessMessageMixin, UpdateView):
  template_name = "createAuthor.html"
  form_class = CreateAuthorModelForm
  model = Author
  success_url = reverse_lazy("booksAuthors")
  success_message = u"%(first_name)s %(last_name)s is changed."
  
updateBookAuthor = UpdateBookAuthor.as_view()

class DeleteBookAuthor(SuccessMessageMixin, DeleteView):
  template_name = "deleteBookAuthor.html"
  model = Author
  success_url = reverse_lazy("booksAuthors")
  success_message = u"%(first_name)s %(last_name)s is deleted."
  
deleteBookAuthor = DeleteBookAuthor.as_view()