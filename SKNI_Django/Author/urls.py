from django.conf.urls import patterns, url

urlpatterns = patterns('SKNI_Django.Author.views',
  url(r'^create/book/author$', 'createBookAuthor', name='createBookAuthor'),
  url(r'^update/book/author/(?P<pk>\d+)$', 'updateBookAuthor', name='updateBookAuthor'),
  url(r'^delete/book/author/(?P<pk>\d+)$', 'deleteBookAuthor', name='deleteBookAuthor'),
  url(r'^books/authors$', 'booksAuthors', name='booksAuthors'),
)
