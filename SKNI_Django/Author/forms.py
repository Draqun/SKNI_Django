from django import forms
from SKNI_Django.Author import models
class CreateAuthorModelForm(forms.ModelForm):
  class Meta:
    model = models.Author
    fields = ['first_name', 'last_name']
    labels = {
              'first_name': "First name",
              'last_name': "Last name"              
             }