from django import forms

class CreateAuthorForm(forms.Form):
  first_name = forms.CharField(max_length=64)
  last_name = forms.CharField(max_length=64)