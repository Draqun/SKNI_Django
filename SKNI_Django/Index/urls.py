from django.conf.urls import patterns, url

urlpatterns = patterns('SKNI_Django.Index.views',
  url(r'^$', 'index', name='index'),
  url(r'^index$', 'index', name='index'),
  url(r'^create/author$', 'createAuthor', name='createAuthor')  
)
