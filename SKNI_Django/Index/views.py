from django.views.generic import TemplateView, FormView
from SKNI_Django.Index.forms import CreateAuthorForm
from django.core.urlresolvers import reverse_lazy
from SKNI_Django.Index.models import Author

class Index(TemplateView):
    template_name="index.html"
    
index = Index.as_view()

class CreateAuthor(FormView):
  template_name = 'createAuthor.html'
  form_class = CreateAuthorForm
  success_url = reverse_lazy('index')
  
  def post(self, request, *args, **kwargs):
    response = super().post(self, request, *args, **kwargs)
    Author(first_name = request.POST["first_name"], last_name = request.POST["last_name"]).save()    
    return response
  
createAuthor = CreateAuthor.as_view()